#include <QDialog>
#include <QString>
#include <QMessageBox>

#include "ui_inventory.h"
#include "components/ingredient.h"

class InventoryDialog : public QDialog, public Ui_diEditInventory {
    Q_OBJECT

	public:
        InventoryDialog(fbc_DB *pDB, QWidget * parent = 0);
		void loadLists();
    private slots:
		void btnAddIngredient();
		void btnDelIngredient();
		void btnClearList();
		
		
    private:
        fbc_DB *db; /**< Database-Connection */
        IngredientListName IngList;// = Ingredient::getListNameUTF8();
};
