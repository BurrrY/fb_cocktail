#include "cocktail.h"
#include "fbc_dbg.h"
bool Cocktail::completeLoaded = false;
bool Cocktail::completeFavLoaded = false;

QMap<int, Cocktail*> Cocktail::cocktailMapID;
QMap<QString, Cocktail*> Cocktail::cocktailMapName;
QList<int> Cocktail::cocktailFavSetID;

Cocktail::Cocktail() { }
Cocktail::~Cocktail() { }



void Cocktail::reloadFavList(fbc_DB *db) {
    cocktailFavSetID.clear();
	int cnt = 0;
    /*
    Database *db = Database::exemplar();
	sqlTable resultData = db->exec("SELECT fav_cocktailId FROM coc_favorites");
	for(sqlRow r: resultData) {
		int ID = atoi(r[0].c_str());
		cocktailFavSetID.insert(ID);
		cnt++;
	}
    */
    DBG_LOAD(cnt << " Favourite Cocktails loaded");
}


void Cocktail::reloadCompleteList(fbc_DB *db) {
	cocktailMapName.clear();
    cocktailMapID.clear();
	int cnt = 0;
	
	//Zutaten-Listen erstellen
    QMap<int, QList<CocktailIngredient>> CocktailIngredientTMP; //Cocktail ID, assigned Ingredients
    QMap<int, QList<CocktailIngredient>>::iterator it;
		 
    FBC_DataSet data = db->getData("SELECT ZOCZ_zutat, ZOCZ_menge, ZOCZ_cocktail, ZOCZ_einheit FROM coc_cocktailzutaten");
    for(FBC_DataRow row: data) {
        int cocktailID = row["ZOCZ_cocktail"].toInt();
        it = CocktailIngredientTMP.find(cocktailID);

        //Check if we can append the list
        QList<CocktailIngredient> *ingredientList;
        if(it == CocktailIngredientTMP.end()) {
            //new
             ingredientList = new QList<CocktailIngredient>();
        } else {
            ingredientList = &CocktailIngredientTMP[cocktailID];
        }

		
        IngredientListID IngList = Ingredient::getListID(db);
        const Ingredient *i = IngList->value(row["ZOCZ_zutat"].toInt());
		
		
        UnitListID unitList = Unit::getList(db);
        const Unit *u = unitList->value(row["ZOCZ_einheit"].toInt());
		
        float m = row["ZOCZ_menge"].toFloat();
		
        CocktailIngredient newCI(i, u, m);
		ingredientList->push_back(newCI);
		
		//do we need to save a new list?
		if(it == CocktailIngredientTMP.end()) {
            CocktailIngredientTMP.insert(cocktailID, *ingredientList);
		} else {
			
		}
		++cnt;
	}
    DBG_LOAD(cnt << "x Ingredients assigned");
	cnt = 0;
	
	
//Deko-Liste
    QMap<int, QList<Decoration*>> DecorationTMP; //Cocktail ID, assigned Ingredients
    QMap<int, QList<Decoration*>>::iterator it2;

    data = db->getData("SELECT ZORD_dekoration, ZORD_rezept FROM coc_cocktaildekoration");
    for(FBC_DataRow row: data) {
        int cocktailID = row["ZORD_rezept"].toInt();
        int dekoID = row["ZORD_dekoration"].toInt();
		it2 = DecorationTMP.find(cocktailID);
		
		//Check if we can append the list
        QList<Decoration*> *DecorationList;
		if(it2 == DecorationTMP.end()) {
			 //new
             DecorationList = new QList<Decoration*>();
        } else {
             DecorationList = &DecorationTMP[cocktailID];
		}
			
        DecorationListID decList = Decoration::getList(db);
        DecorationList->push_back((*decList)[dekoID]);
		
		//do we need to save a new list?
		if(it2 == DecorationTMP.end()) {
            DecorationTMP.insert(cocktailID, *DecorationList);
		} else {
			
		}
		++cnt;
	}
    DBG_LOAD(cnt << "x Decorations assigned.");
	cnt = 0;


//COCKTAILS	
    CocktailFavSet FavSet = Cocktail::getFavList(db);
    data = db->getData("SELECT coc_id, coc_name, coc_beschreibung, coc_alkohol, coc_kategorie FROM coc_cocktails");
     for(FBC_DataRow row: data) {
		
		Cocktail *c = new Cocktail();		
        c->ID = row["coc_id"].toInt();
        c->Name = row["coc_name"].toString();
        c->Description = row["coc_beschreibung"].toString();
        c->Alc = row["coc_alkohol"].toInt();
        c->isFav = FavSet->contains(c->ID);
		
	
		//get Category		
		CategoryListID catList = Category::getList();
        c->category = (*catList)[row["coc_kategorie"].toInt()];
		
        //Assign Ingredients
        if(CocktailIngredientTMP.contains(c->ID)) {
            c->ingredients = (CocktailIngredientTMP)[c->ID];
		}
		
        //Assign Decorations
        if(DecorationTMP.contains(c->ID)) {
            c->decorations = DecorationTMP[c->ID];
		}

        cocktailMapName.insert(c->Name.toLower(), c);
        cocktailMapID.insert(c->ID, c);
        DBG_LOAD2("Cocktail " << c->Name << " loaded. " << c->ID << "|" << cocktailMapID.count());
		++cnt;
	}	
	
	completeLoaded = true;
	
    DBG_LOAD(cnt << " COCKTAILS loaded");
}

QString Cocktail::decorationList() {
        QString dec;
		for(Decoration *d : decorations) {
			dec += d->Name;
		}
		
		
		return dec;
}

bool Cocktail::hasIngredient(QString ingredientName) {
    for(CocktailIngredient ci : ingredients) {
            if(ci.ingredient->Name == ingredientName) {
				return true;
			}
	}	
	
	return false;
}

bool Cocktail::hasIngredient(Ingredient *i) {

    for(CocktailIngredient ci : ingredients) {
            if(ci.ingredient == i) {
				return true;
			}
	}	
	
	return false;
}

float Cocktail::totalMass() {
	float mass = 0;
    for(CocktailIngredient ci : ingredients) {
        if(ci.unit->isFluid) {
            mass += ci.Mass * ci.unit->inMl;
		} else {
            qDebug() << ci.unit->Name <<  " not fluid";
		}
	}
	
	return mass/10;
}

QList<int>*  Cocktail::getFavList(fbc_DB *db) {
	if(!completeFavLoaded) {
            reloadFavList(db);
			completeFavLoaded = true;
	}	
	
	return &cocktailFavSetID;
}


QMap<QString, Cocktail*>*  Cocktail::getListName(fbc_DB *db) {
	if(!completeLoaded) {
        reloadCompleteList(db);
		completeLoaded = true;
	}
	
    return &cocktailMapName;
}

Cocktail *Cocktail::getCocktail(QString Name, fbc_DB *db)
{
    QMap<QString, Cocktail*> *map = getListName(db);
    Cocktail *c = map->value(Name.toLower());
    if(!c)
      throw 10;

    return c;
}


QMap<int, Cocktail*>*  Cocktail::getListID(fbc_DB *db) {
	if(!completeLoaded) {
        reloadCompleteList(db);
		completeLoaded = true;
	}
	
	return &cocktailMapID;
}

QString Cocktail::toIngredientTable() {
    QString result = "<table>";
    for(CocktailIngredient ci : ingredients) {
        if(ci.ingredient->Alc > 0 ) {
            result += "<tr> <th>" + ci.ingredient->Name + " (" + QString::number(ci.ingredient->Alc, 'g', 2) +"%)</th> <td>" + QString::number(ci.Mass,'g', 2) + " " + (ci.unit->Abrev) + "</td> </tr>";
		} else {
            result += "<tr> <th>" + ci.ingredient->Name + "</th> <td>" + QString::number(ci.Mass,'g', 2) + " " + (ci.unit->Abrev) + "</td> </tr>";
		}
		
	}
	result += "</table>";
	
	return result;
}
