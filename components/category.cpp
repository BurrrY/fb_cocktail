#include "category.h"
#include "fbc_dbg.h"
Category::Category() { }
Category::~Category() { }

bool Category::loaded = false;
QMap<int, Category*> Category::categoryMapID;
QMap<QString, Category*> Category::categoryMapName;

Category::Category(int ID, QString Name) {
		this->ID = ID;
		this->Name = Name;
}


QMap<int, Category *> *Category::getList() {
	if(!Category::loaded) {
        //reloadList();
		loaded = true;		
	}	
	return &categoryMapID;
}



QMap<QString, Category*>* Category::getListName() {
	if(!Category::loaded) {
       // reloadList();
		loaded = true;		
	}	
	return &categoryMapName;
}

void Category::reloadList(fbc_DB *db) {

	categoryMapID.clear();
    categoryMapName.clear();
    int cnt = 0;
    FBC_DataSet data = db->getData("SELECT kat_id, kat_name, kat_beschreibung FROM coc_kategorien");
    for(FBC_DataRow row: data) {

        Category *c = new Category();
        c->ID = row["kat_id"].toInt();
        c->Name = row["kat_name"].toString();
        c->Desc = row["kat_beschreibung"].toString();
        Category::categoryMapID.insert(c->ID, c);
        Category::categoryMapName.insert(c->Name, c);
        ++cnt;
	}	
    DBG_LOAD(cnt << "Categories Loaded");
    loaded = true;
}
