#ifndef __COCKTAIL_H__
#define __COCKTAIL_H__


#include <QString>
#include "ingredient.h"
#include "cocktailIngredient.h"
#include "category.h"
#include "decoration.h"



class Cocktail {
	public:
		Cocktail();
      //  Cocktail(const Cocktail& other);
		~Cocktail();
	

        int ID;
        QString Name;
        QString Description;
        QString Picture;
        int Counter;
        int Alc;
        bool isFav;

        Category *category;

        //ice Ice = ice.AllByID[4];
        //flavor Flavor = flavor.AllByID[9];

        
        QList<CocktailIngredient> ingredients;
        QList<Ingredient*> missingIngredients;
        QList<Decoration*> decorations;
        QString toIngredientTable();
		

		//getter
		float totalMass();
        QString decorationList();
        bool hasIngredient(QString ingredientName);
		bool hasIngredient(Ingredient *i);		
		int getId() const;
		
        //Operator
		bool operator==(const Cocktail& n) {return (getId() == n.getId());}

        //Static
        static QMap<int, Cocktail*>*  getListID(fbc_DB *db);
        static QList<int>*  getFavList(fbc_DB *db);
        static QMap<QString, Cocktail*>*  getListName(fbc_DB *db);
        static Cocktail *getCocktail(QString Name, fbc_DB *db);
	private:
		
        static void reloadCompleteList(fbc_DB *db);
        static void reloadFavList(fbc_DB *db);
		static bool completeLoaded;
		static bool completeFavLoaded;
		
        static QMap<QString, Cocktail*> cocktailMapName;
        static QMap<int, Cocktail*> cocktailMapID;
        static QList<int> cocktailFavSetID;
};



typedef QMap<int, Cocktail*>* CocktailListID;
typedef QMap<QString, Cocktail*>* CocktailListName;
typedef QList<int>* CocktailFavSet;

#endif
