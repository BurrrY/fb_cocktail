#include "unit.h"
#include "fbc_dbg.h"

bool Unit::loaded = false;
QMap<int, Unit*> Unit::unitMapID;
QMap<QString, Unit*> Unit::unitMapName;

Unit::Unit() { }
Unit::~Unit() { }

Unit::Unit(QString Name, QString Abrev, int ID, bool fluid, int ml) {
	this->Name = Name;
	this->Abrev = Abrev;
	this->ID = ID;
	this->isFluid = fluid;
	this->inMl = ml;
}

UnitListID Unit::getList(fbc_DB *db) {
	if(!Unit::loaded) {
        reloadList(db);
		loaded = true;
	}	
	return &unitMapID;
}

UnitListName Unit::getListName(fbc_DB *db) {
	if(!Unit::loaded) {
        reloadList(db);
		loaded = true;
	}	
	return &unitMapName;
}



void Unit::reloadList(fbc_DB *db) {
	int cnt = 0;
	unitMapID.clear();
	unitMapName.clear();

    FBC_DataSet data = db->getData("SELECT ein_id, ein_name, ein_kuerzel, ein_fluessig, ein_ml FROM coc_einheiten");
    for(FBC_DataRow row: data) {
        Unit *u = new Unit();
        u->ID = row["ein_id"].toInt();
        u->Name = row["ein_name"].toString();
        u->Abrev = row["ein_kuerzel"].toString();
        u->isFluid = row["ein_fluessig"].toBool();
        u->inMl = row["ein_ml"].toInt();

        Unit::unitMapID.insert(u->ID, u);
        Unit::unitMapName.insert(u->Name, u);
        ++cnt;
    }


	
	loaded = true;
    DBG_LOAD(cnt << " UNITS loaded");

}
