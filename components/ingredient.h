#ifndef __INGREDIENT_H__
#define __INGREDIENT_H__


#include <QVariant>
#include <QString>
#include <qdebug.h>

#include "category.h"

class Ingredient {
	public:

		Ingredient();
		~Ingredient();
		

        int ID;
        QString Name;// = "";
        QString Description;// = "";
        float Alc;// = 0;
        Category *category;

        bool IsInSilo;// = false;
        int TimesUsed;// = 0;
        
        float MassUsed;// = 0;
        bool IsFluid;// = false;
        float RequiredMass;// = 0;

        int TimesMissed;// = 0; //only for shoppingList
		
		//Static Methods
        static QList<int>*  getAvailableList(fbc_DB *db, bool forceReload = false);
        static QMap<int, Ingredient*>*  getListID(fbc_DB *db);
        static QMap<QString, Ingredient*>*  getListName(fbc_DB *db);
        static void reloadCompleteList(fbc_DB *db);

	private:
        static void reloadAvailableList(fbc_DB *db);
        static QList<int> ingredientAvailableID;
		static bool availableLoaded;

		static bool completeLoaded;
			
        static QMap<int, Ingredient*> ingredientMapID;
        static QMap<QString, Ingredient*> ingredientMapName;
};

typedef QList<int>* IngredientAvailableListID;

typedef QMap<int, Ingredient*>* IngredientListID;

typedef QMap<QString, Ingredient*>* IngredientListName;

#endif
