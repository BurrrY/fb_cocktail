#ifndef __UNIT_H__
#define __UNIT_H__

#include <QMap>
#include <QString>
#include  <QVariant>
#include <qdebug.h>
#include <fbc_db.h>


class Unit {
	public:
        Unit(QString Name, QString Abrev, int ID, bool fluid, int ml);
		Unit();
		~Unit();
		
		//Static Methods
        static QMap<int, Unit*>*  getList(fbc_DB *db);
        static QMap<QString, Unit*>*  getListName(fbc_DB *db);
        static void reloadList(fbc_DB *db);

        QString Name;
        QString Abrev;
        bool isFluid;
        int inMl;
        int ID;
		
    private:
        //Static Members
        static bool loaded;
        static QMap<int, Unit*> unitMapID;
        static QMap<QString, Unit*> unitMapName;
	

};

typedef QMap<int, Unit*>* UnitListID;
typedef QMap<QString, Unit*>* UnitListName;
#endif
