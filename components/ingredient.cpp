#include "ingredient.h"
#include "fbc_dbg.h"

bool Ingredient::availableLoaded = false;
bool Ingredient::completeLoaded = false;
	
QList<int>             Ingredient::ingredientAvailableID;
QMap<int, Ingredient*> Ingredient::ingredientMapID;
QMap<QString, Ingredient*>   Ingredient::ingredientMapName;


Ingredient::Ingredient() { }
Ingredient::~Ingredient() {  }

void Ingredient::reloadAvailableList(fbc_DB *db) {
	ingredientAvailableID.clear();
	int cnt = 0;

    FBC_DataSet data = db->getData("SELECT fla_zutat FROM coc_flaschenmagazin WHERE fla_modul = 1");
    for(FBC_DataRow row: data) {
        ingredientAvailableID.append(row["fla_zutat"].toInt());
        ++cnt;

    }

	
	availableLoaded = true;
    DBG_LOAD(cnt << " AvailableIngredients loaded");
}

QMap<int, Ingredient*>*  Ingredient::getListID(fbc_DB *db){
	if(!completeLoaded) {
        reloadCompleteList(db);
		completeLoaded= true;
	}
	
	return &ingredientMapID;
}


QMap<QString, Ingredient*>*  Ingredient::getListName(fbc_DB *db){
	if(!completeLoaded) {
        reloadCompleteList(db);
		completeLoaded= true;
	}
	
	return &ingredientMapName;
}




QList<int>*  Ingredient::getAvailableList(fbc_DB *db, bool forceReload){
	if(!availableLoaded || forceReload) {
        reloadAvailableList(db);
		availableLoaded= true;
	}	
	return &ingredientAvailableID;
}

void Ingredient::reloadCompleteList(fbc_DB *db) {
	ingredientMapID.clear();
    ingredientMapName.clear();
	int cnt = 0;
    FBC_DataSet data = db->getData("SELECT zut_id, zut_name, zut_alkgehalt, zut_beschreibung, zut_dosieren, zut_verwendet, zut_kategorie FROM coc_zutaten");
    for(FBC_DataRow row: data) {
        Ingredient *i = new Ingredient();
        i->ID = row["zut_id"].toInt();
        i->Name = row["zut_name"].toString();
        i->Description = row["zut_beschreibung"].toString();

        i->Alc = row["zut_alkgehalt"].toFloat();

        i->TimesUsed = row["zut_verwendet"].toInt();

        CategoryListID catList = Category::getList();
        if(catList->contains(row["zut_kategorie"].toInt()))
            i->category = catList->value(row["zut_kategorie"].toInt());
        else
            qWarning() << "Cannot find Category ID " << row["zut_kategorie"].toInt();

        //Ingredient ID, Ingredient
        ingredientMapID.insert(i->ID, i);

        //Ingredient Name lowercase, Ingredient
        ingredientMapName.insert(i->Name, i);
        ++cnt;
    }
	
	completeLoaded= true;
    DBG_LOAD(cnt << " Ingredients loaded");
}

