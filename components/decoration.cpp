#include "decoration.h"
#include "fbc_db.h"
#include "fbc_dbg.h"

bool Decoration::loaded = false;
QMap<int, Decoration*> Decoration::decorationMapID;
QMap<QString, Decoration*> Decoration::decorationMapName;

Decoration::Decoration() { }
Decoration::~Decoration() { }

Decoration::Decoration(QString Name, QString desc, int ID) {
	this->Name = Name;
	this->ID = ID;
	this->Desc = desc;
}

QMap<int, Decoration *> *Decoration::getList(fbc_DB *db) {
	if(!Decoration::loaded) {
        reloadList(db);
		loaded = true;		
	}	
	return &decorationMapID;
}


QMap<QString, Decoration*>* Decoration::getListName(fbc_DB *db) {
	if(!Decoration::loaded) {
        reloadList(db);
		loaded = true;		
	}	
	return &decorationMapName;
}




void Decoration::reloadList(fbc_DB *db) {
    int cnt = 0;

    decorationMapID.clear();
    decorationMapName.clear();

    FBC_DataSet data = db->getData("SELECT dek_id, dek_name, dek_beschreibung FROM coc_dekoration");
    for(FBC_DataRow row: data) {

        Decoration *d = new Decoration();
        d->ID = row["dek_id"].toInt();
        d->Name = row["dek_name"].toString();
        d->Desc = row["dek_beschreibung"].toString();
        Decoration::decorationMapID.insert(d->ID, d);
        Decoration::decorationMapName.insert(d->Name, d);
        ++cnt;
    }

    loaded = true;
    DBG_LOAD(cnt << " DECORATIONS loaded");
	
}
