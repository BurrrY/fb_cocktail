#ifndef __DECORATION_H__
#define __DECORATION_H__

#include <QMap>
#include <QString>
#include <fbc_db.h>
#include "qdebug.h"

class Decoration {
	public:
		//Methods
        Decoration(QString Name, QString desc, int ID);
		Decoration();
		~Decoration();

		//Static Methods
        static QMap<int, Decoration*>*  getList(fbc_DB *db);
        static QMap<QString, Decoration *> *getListName(fbc_DB *db);
        static void reloadList(fbc_DB *db);
		
		//Members
        QString Name;
        QString Desc;
        int ID;
        
    
    private:
         static bool loaded; 
         
        //Static Members
        static QMap<int, Decoration*> decorationMapID;
        static QMap<QString, Decoration*> decorationMapName;
};

typedef QMap<int, Decoration*>* DecorationListID;
typedef QMap<QString, Decoration*>* DecorationListName;


#endif
