#include "slot.h"

bool Slot::Loaded = false;

QMap<int, Slot*> Slot::SlotMapID;

Slot::Slot()
{

}
Slot::Slot(int pos,int mAmount,int cAmount, Ingredient *i)
{
    position = pos;
    maxAmount = mAmount;
    currentAmount = cAmount;
    ingredient = i;
}
int Slot::getMaxAmount() const
{
    return maxAmount;
}

void Slot::setMaxAmount(int value)
{
    maxAmount = value;
}
int Slot::getCurrentAmount() const
{
    return currentAmount;
}

void Slot::setCurrentAmount(int value)
{
    currentAmount = value;
}
int Slot::getPosition() const
{
    return position;
}

void Slot::setPosition(int value)
{
    position = value;
}
Ingredient *Slot::getIngredient() const
{
    return ingredient;
}

void Slot::setIngredient(Ingredient *value)
{
    ingredient = value;
}

QMap<int, Slot *> *Slot::getListID(fbc_DB *db)
{
    if(!Loaded)
    {
        loadSlots(db);
    }

    return &SlotMapID;
}

void Slot::loadSlots(fbc_DB *db)
{
    SlotMapID.clear();
    int cnt = 0;

    QMap<int, Ingredient*> *i = Ingredient::getListID(db);
    FBC_DataSet data = db->getData("SELECT sl_id, sl_level, sl_size, sl_ingredient, sl_port FROM coc_slots");
    for(FBC_DataRow row: data) {
        Slot *s = new Slot();
        s->setCurrentAmount(row["sl_level"].toInt());
        s->setIngredient(i->value(row["sl_ingredient"].toInt()));
        s->setMaxAmount(row["sl_size"].toInt());
        s->setPosition(row["sl_port"].toInt());
        SlotMapID.insert(row["sl_id"].toInt(), s);
        ++cnt;
    }
    qDebug() << cnt << "Slots loaded.";
    Loaded = true;
}

Slot* Slot::getSlotForIngredientID(int ID) {


    for(Slot* s:SlotMapID) {
        if(s->getIngredient()->ID == ID)
            return s;
    }

    return NULL;
}





