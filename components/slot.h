#ifndef SLOT_H
#define SLOT_H

#include "ingredient.h"



class Slot
{
public:
    Slot();
    Slot(int pos, int mAmount, int cAmount, Ingredient *i);

    int getMaxAmount() const;
    void setMaxAmount(int value);

    int getCurrentAmount() const;
    void setCurrentAmount(int value);

    int getPosition() const;
    void setPosition(int value);

    Ingredient *getIngredient() const;
    void setIngredient(Ingredient *value);

    static QMap<int, Slot*>*  getListID(fbc_DB *db);

    static Slot *getSlotForIngredientID(int ID);
private:
    Ingredient *ingredient;
    int maxAmount;
    int currentAmount;
    int position;

    static bool Loaded;
    static QMap<int, Slot*> SlotMapID;
    static void loadSlots(fbc_DB *db);
};

#endif // SLOT_H
