#ifndef __CocktailIngredient_H__
#define __CocktailIngredient_H__

#include "ingredient.h"
#include "unit.h"

class CocktailIngredient {
	public:
		CocktailIngredient();
        CocktailIngredient(const Ingredient *i, const Unit *u, float mass);
		~CocktailIngredient();
		
                const Ingredient* ingredient;
                const Unit *unit;
        float Mass;

};



#endif
