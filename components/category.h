#ifndef __CATEGORY_H__
#define __CATEGORY_H__

#include <QString>
#include <QMap>
#include <fbc_db.h>

class Category {
	public:
		Category();
		~Category();
		
        Category(int ID, QString Name);
		
        int ID;
        QString Name;
        QString Desc;
        
		//Static Methods
        static QMap<int, Category*>* getList();
        static QMap<QString, Category*>* getListName();
        static void reloadList(fbc_DB *db);
        
	private:
        //Static Methods
        static bool loaded;
        //Static Members
        static QMap<int, Category*> categoryMapID;
        static QMap<QString, Category*> categoryMapName;


};

typedef QMap<int, Category*>* CategoryListID;
typedef QMap<QString, Category*>* CategoryListName;

#endif
