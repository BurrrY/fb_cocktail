#ifndef MIXINGDIALOGWIDGET_H
#define MIXINGDIALOGWIDGET_H

#include <QWidget>

#include <components/cocktailIngredient.h>

namespace Ui {
class MixingDialogWidget;
}

class MixingDialogWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MixingDialogWidget(CocktailIngredient *i, QWidget *parent = 0);
    ~MixingDialogWidget();

    void setNr(int nr);

    void updateIngredientMass();
signals:
    void massValueChanged(double i, CocktailIngredient *ci);

private slots:
    void on_dsb_MassCL_valueChanged(double val);

private:
    Ui::MixingDialogWidget *ui;
    CocktailIngredient *i;
};

#endif // MIXINGDIALOGWIDGET_H
