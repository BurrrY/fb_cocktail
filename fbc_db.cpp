#include "fbc_db.h"

#include <QSqlQuery>
#include <QSqlRecord>
#include "QVariant"

fbc_DB::fbc_DB()
{

}



bool fbc_DB::connnect(){
    QFile dbFile("Cocktail.db");
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName("localhost");

    db.setDatabaseName(QFileInfo(dbFile).absoluteFilePath());
    return db.open();
}

FBC_DataSet fbc_DB::getData(QString query) {
    QSqlQuery q;

    QList<QMap<QString, QVariant>>  result;
    q.exec(query);
    while(q.next())
    {
       QSqlRecord r = q.record();
       QMap<QString, QVariant> row;
       for(int i=0;i<r.count(); ++i)
           row.insert(r.fieldName(i), r.value(i));

       result.append(row);
    }
    return result;
}

bool fbc_DB::exec(QString query) {
    QSqlQuery q;
    return q.exec(query);
}
