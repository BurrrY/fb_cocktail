#include "fb_cocktail.h"
#include "ui_fb_cocktail.h"
#include "fbc_dbg.h"
#include "FilterDialog.h"
#include "slotdialog.h"
#include "mixingdialog.h"

#include <QFontDatabase>

FB_Cocktail::FB_Cocktail(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FB_Cocktail)
{
    ui->setupUi(this);
    db = new fbc_DB();

    if(!db->connnect())
        qWarning() << "Could not connect to DB!";

    //load Fonts
    QStringList list;
    list << "Lato-Regular.ttf" << "Lato-Light.ttf";
    int fID;
    for (QStringList::const_iterator cIt = list.constBegin(); cIt != list.constEnd(); ++cIt)
    {
        QFile res(":/fonts/" + *cIt);
        if (res.open(QIODevice::ReadOnly) == false)
        {
            QMessageBox::warning(0, "Font Loader", (QString)"Cannot open font: " + *cIt + ".");
        }
        else
        {
            fID = QFontDatabase::addApplicationFontFromData(res.readAll());
            if (fID == -1)
                QMessageBox::warning(0, "Font Loader", (QString)"Cannot load font: " + *cIt + ".");
        }
    }

    //set Font
    if(fID >= 0) {
        QFontDatabase fodb;
        QFont latoFont = fodb.font("Lato Regular", "Regular", 10);
        qApp->setFont(latoFont);


        QFont latoFontLight = fodb.font("Lato Light", "Light", 24);
  //     ui->le_Title->setFont(latoFontLight);
     }



    Category::reloadList(db);
    Decoration::reloadList(db);
    Unit::reloadList(db);
    Ingredient::reloadCompleteList(db);
    Cocktail::getListID(db);
    Slot::getListID(db);

    reloadList(Cocktails);
    selectedList = Cocktails;


    QString stylesheet = ":/stylesheet.qss";
    if(!stylesheet.isNull())
    {
        QFile stylesheetFile(stylesheet);
        stylesheetFile.open(QFile::ReadOnly);
        QString currentStyle = QLatin1String(stylesheetFile.readAll());
        if(!currentStyle.isNull())
        {
            qDebug("Stylesheet loaded.");
            qApp->setStyleSheet(currentStyle);
        }
    }
}

FB_Cocktail::~FB_Cocktail()
{
    delete db;

    delete ui;
}

void FB_Cocktail::reloadList(selectables item) {
    selectedList = item;
    ui->lw_mainList->clear();

    switch(item){
        case Cocktails:
        {
            QList<Cocktail *> lst = Cocktail::getListID(db)->values();
            for(Cocktail *c : lst) {
                ui->lw_mainList->addItem(c->Name);
            }
        }
        break;
    case Ingredients:
        {
            QList<Ingredient *> lst = Ingredient::getListID(db)->values();
            for(Ingredient *c : lst) {
                ui->lw_mainList->addItem(c->Name);
            }
        }
        break;
    case FilterResult:
        selectedList = Cocktails;
        QList<Cocktail *> lst = tmpList;
        for(Cocktail *c : lst) {
            ui->lw_mainList->addItem(c->Name);
        }

        break;
    }
}

void FB_Cocktail::on_btn_tb_Cocktails_clicked()
{
    reloadList(Cocktails);
}

void FB_Cocktail::on_btn_tb_Ingredients_clicked()
{
    reloadList(Ingredients);
}

void FB_Cocktail::on_btn_tb_possibleRecepies_clicked()
{
    //Available Ingredients

    QSet<int> badOnes;
    selectedList = Cocktails;
    int cnt = 0;
    ui->lw_mainList->clear();


    IngredientAvailableListID ingAvList = Ingredient::getAvailableList(db);
    QList<Cocktail *> CockList = Cocktail::getListID(db)->values();

    for(Cocktail *kv : CockList)  //Iterate through cocktails
        for(CocktailIngredient ci : kv->ingredients)  //Iterate through ingredients
            if(ingAvList->contains(ci.ingredient->ID))  //If ingredient not available, remove
                badOnes.insert(kv->ID);




    for(Cocktail* kv: CockList) {
        if(!badOnes.contains(kv->ID)) {
            QListWidgetItem *it = new QListWidgetItem(kv->Name);
            if(kv->isFav) {
                it->setForeground(QBrush(QColor(255,0,0)));
            }
            ui->lw_mainList->addItem(it);
            ++cnt;
        }
    }
}

void FB_Cocktail::on_btn_tb_possibleOnAuto_clicked()
{

}

void FB_Cocktail::on_btn_tb_homebar_clicked()
{
  InventoryDialog ivDialog(db);
    ivDialog.exec();

  // Ingredient::getAvailableList(true);
}

void FB_Cocktail::on_btn_tb_filter_clicked()
{
    FilterDialog d(db, &tmpList);
    d.exec();

    reloadList(FilterResult);
}

void FB_Cocktail::on_btn_tb_slots_clicked()
{
    SlotDialog d(db);

    d.exec();
}

void FB_Cocktail::on_btn_tb_settings_clicked()
{

}

void FB_Cocktail::on_pbMixing_clicked()
{
    if(selectedList != Cocktails || ui->lw_mainList->selectedItems().length()!=1)
        return;

    QString selectedCocktail = ui->lw_mainList->selectedItems().at(0)->text();
    Cocktail *c = Cocktail::getCocktail(selectedCocktail, db);
    MixingDialog md(db, c);
    md.exec();
}

void FB_Cocktail::on_act_ShoppingList_triggered()
{

}

void FB_Cocktail::on_act_db_Cocktails_triggered()
{

}

void FB_Cocktail::on_act_db_Ingredients_triggered()
{

}

void FB_Cocktail::on_act_db_Categories_triggered()
{

}

void FB_Cocktail::on_act_db_units_triggered()
{

}

void FB_Cocktail::on_act_db_decoration_triggered()
{

}

void FB_Cocktail::on_lw_mainList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if(!current)
        return;

    DBG_GUI("Selected Item changed to: " << current->text());

    QString desc = "";
    if(selectedList == Cocktails) {
        Cocktail *c = (*Cocktail::getListName(db)).value(current->text().toLower());

        if(!c) return;

        ui->le_Title->setText(c->Name);

        if(c->isFav)
            ui->pbFav->setText("- Fav");
        else
            ui->pbFav->setText("+ Fav");


        desc = "";
        desc += "<table><tr><td  width=300 >";
        desc += "<h2>Zutaten</h2>";
        desc += c->toIngredientTable();
        desc += "</td><td width=10 ></td><td style=\"background-color:black;\"></td><td width=10 ></td><td>";

        desc += "<h2>Details</h2>";
        desc += "<dl>";
        desc += "<dt>Menge</dt><dd>" + QString::number(c->totalMass()) + "cl</dd>";
        desc += "<dt>Alkohol</dt><dd>" + QString::number(c->Alc) + "%</dd>";

        if(c->category->ID != 30) { // 30 = n.A.
            desc += "<dt>Kategorie</dt><dd>" + c->category->Name + "</dd>";
        }

        if(c->decorations.size() > 0) {
            desc += "<dt>Dekoration</dt><dd>" + c->decorationList() + "</dd>";
        }

        desc += "</dl></tr></table>";
        desc += "<hr><h2>Beschreibung</h2>";
        desc += "<p>" + c->Description + "</p>";
    }

    ui->te_mainText->setText(desc);

}

void FB_Cocktail::on_leSearch_textChanged(const QString term)
{
    ui->lw_mainList->clear();
    int cnt = 0;

    QString lowerName = term.toLower();

    switch(selectedList) {
        case Cocktails:
            {
                CocktailListName CockList = Cocktail::getListName(db);
                for(Cocktail *kv : *CockList) {
                    if(kv->Name.toLower().contains(lowerName)) {
                        ui->lw_mainList->addItem(kv->Name);
                        cnt++;
                    }
                }
            }
        break;
        case Ingredients:
            IngredientListName IngList = Ingredient::getListName(db);
            for(Ingredient *kv : *IngList) {
                if(kv->Name.contains(lowerName)) {
                    ui->lw_mainList->addItem(kv->Name);
                    cnt ++;
                }
            }

        break;
    }
}
