#-------------------------------------------------
#
# Project created by QtCreator 2015-09-04T15:56:03
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FB_Cocktail
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++0x
RC_FILE = res.rc

SOURCES += main.cpp\
        fb_cocktail.cpp \
    fbc_db.cpp \
    components/category.cpp \
    components/decoration.cpp \
    components/unit.cpp \
    components/ingredient.cpp \
    components/cocktail.cpp \
    components/cocktailIngredient.cpp \
    homeInvetory.cpp \
    FilterDialog.cpp \
    slotdialog.cpp \
    components/slot.cpp \
    mixingdialog.cpp \
    mixingdialogwidget.cpp \
    output.cpp

HEADERS  += fb_cocktail.h \
    fbc_db.h \
    components/category.h \
    components/decoration.h \
    components/unit.h \
    components/ingredient.h \
    components/cocktail.h \
    components/cocktailIngredient.h \
    fbc_dbg.h \
    homeInvetory.h \
    FilterDialog.h \
    slotdialog.h \
    components/slot.h \
    mixingdialog.h \
    mixingdialogwidget.h \
    output.h

FORMS    += fb_cocktail.ui \
    inventory.ui \
    Filter.ui \
    slotdialog.ui \
    mixingdialog.ui \
    mixingdialogwidget.ui

DISTFILES += \
    stylesheet.qss

RESOURCES += \
    resources.qrc
