#ifndef MIXINGDIALOG_H
#define MIXINGDIALOG_H

#include "mixingdialogwidget.h"

#include <QDialog>
#include <QSpacerItem>

#include <components/cocktail.h>

namespace Ui {
class MixingDialog;
}

class MixingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MixingDialog(fbc_DB *db, Cocktail *c, QWidget *parent = 0);
    ~MixingDialog();

private:
    Ui::MixingDialog *ui;
    fbc_DB *db;
    Cocktail *c;
    Cocktail *c_cpy;

    QLayout *scrollLayout;
    QSpacerItem *vSpacer;

    QList<MixingDialogWidget*> ingredientPanels;

    void loadCocktail();
private slots:
    void ingredientMassChanged(double i, CocktailIngredient *ci);
    void on_dsb_Mass_valueChanged(double val);
    void on_pb_reset_clicked();
};

#endif // MIXINGDIALOG_H
