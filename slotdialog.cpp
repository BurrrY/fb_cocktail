#include "slotdialog.h"
#include "ui_slotdialog.h"
#include "fbc_dbg.h"
#include <QLabel>
#include <QProgressBar>

SlotDialog::SlotDialog(fbc_DB *db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SlotDialog)
{
    ui->setupUi(this);
    ui->tw_SlotOverview->clear();

#ifdef SLOT_SIMULATION
    numSlots = 8;
    QList<Ingredient *> ing = Ingredient::getListID(db)->values();
    for(int i=0;i<numSlots;++i) {
        Slot s(i,1000, 124,ing.at(i));
        Slots.append(s);
    }
#else


#endif


    QList<Ingredient *> ingA = Ingredient::getListID(db)->values();

    ui->tw_SlotOverview->setRowCount(numSlots);
    ui->tw_SlotOverview->setColumnCount(5); //Pos, Ingredient, Current, Max, Bar
    ui->tw_SlotOverview->setContentsMargins(3,3,3,3);


    int i=0;
    for(Slot s : Slots) {
        QProgressBar *p = new QProgressBar(this);
        p->setMaximum(s.getMaxAmount());
        p->setValue(s.getCurrentAmount());

        QComboBox *ingredientBox = new QComboBox();
        for(Ingredient *i: ingA) {
            ingredientBox->addItem(i->Name, i->ID);
        }

        int idx = ingredientBox->findText(s.getIngredient()->Name);
        ingredientBox->setCurrentIndex(idx);
        ui->tw_SlotOverview->setCellWidget(i,0,new QLabel(QString::number(s.getPosition()+1)));
        ui->tw_SlotOverview->setCellWidget(i,1, ingredientBox);
        ui->tw_SlotOverview->setItem(i,2,new QTableWidgetItem(QString::number(s.getCurrentAmount())));
        ui->tw_SlotOverview->setCellWidget(i,3,p);
        ui->tw_SlotOverview->setItem(i++,4,new QTableWidgetItem(QString::number(s.getMaxAmount())));
        QStringList headerItems;
        headerItems << "ID" << "Ingredient" << "Available" << "" << "Max";
        ui->tw_SlotOverview->setHorizontalHeaderLabels(headerItems);
    }
}

SlotDialog::~SlotDialog()
{
    delete ui;
}
