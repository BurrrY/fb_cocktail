#ifndef FBC_DBG
#define FBC_DBG

#include "qdebug.h"

#define SLOT_SIMULATION

//Globally EN/DIS-Able DBG-Outputs
#define DBG_PRINT_ENABLED

//Enable Debug for Sections
#ifdef DBG_PRINT_ENABLED

//Loading Stuff on startup
    #define DBG_LOAD_EN
    //#define DBG_LOAD2_EN

//Print GUI-In/Outputs
    #define DBG_GUI_EN


#endif


//Macros for Each Debug-Section below
#ifdef DBG_GUI_EN
    #define DBG_GUI(x) qDebug() << x
#else
    #define DBG_GUI(x)
#endif


#ifdef DBG_LOAD_EN
    #define DBG_LOAD(x) qDebug() << x
#else
    #define DBG_LOAD(x)
#endif


#ifdef DBG_LOAD2_EN
    #define DBG_LOAD2(x) qDebug() << x
#else
    #define DBG_LOAD2(x)
#endif


#endif // FBC_DBG

