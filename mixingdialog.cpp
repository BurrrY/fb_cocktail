#include "mixingdialog.h"
#include "ui_mixingdialog.h"

MixingDialog::MixingDialog(fbc_DB *db, Cocktail *c, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MixingDialog)
{
    ui->setupUi(this);
    this->db = db;
    this->c = c;
    this->c_cpy = new Cocktail(*c);



    //Scrolling-List for Ingredients
    scrollLayout = new QVBoxLayout();
    scrollLayout->setMargin(1);
    scrollLayout->setSpacing(0);
    ui->scrollArea->widget()->setLayout(scrollLayout);

    vSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

    loadCocktail();



}

void MixingDialog::loadCocktail()
{
    ui->dsb_Mass->blockSignals(true);


    ui->le_Title->setText(c_cpy->Name);
    ui->dsb_Mass->setValue(c_cpy->totalMass() / 100);

    for (MixingDialogWidget *w : ingredientPanels)
    {
        scrollLayout->removeWidget(w);
        delete(w);
    }
    scrollLayout->removeItem(vSpacer);

    ingredientPanels.clear();

    int cnt = 0;
    for(int i=0; i< c_cpy->ingredients.length(); ++i) {
        MixingDialogWidget *wg = new MixingDialogWidget(&c_cpy->ingredients[i], ui->scrollArea);
        wg->setNr(++cnt);
     //   wg->deleteLater();
        connect(wg, SIGNAL(massValueChanged(double, CocktailIngredient*)), this, SLOT(ingredientMassChanged(double, CocktailIngredient*)));
        scrollLayout->addWidget(wg);
        ingredientPanels.append(wg);
    }


    scrollLayout->addItem(vSpacer);
    ui->dsb_Mass->blockSignals(false);
}

MixingDialog::~MixingDialog()
{
    delete ui;
}

void MixingDialog::ingredientMassChanged(double i, CocktailIngredient *ci)
{
    ui->dsb_Mass->setValue(c_cpy->totalMass()/100);
}

void MixingDialog::on_dsb_Mass_valueChanged(double val)
{
    double oldMass = c_cpy->totalMass();
    double newMass = val*100;
    double deltaMass = newMass - oldMass;

    //CHeck if we can increase the maxAmout
    for(int i = 0;i<c_cpy->ingredients.length();++i){
        double prop = (100/oldMass)  * c_cpy->ingredients.value(i).Mass;
        double propPart = deltaMass * (prop/100);
        double newMass = c_cpy->ingredients.value(i).Mass + propPart;

        if (newMass > 99)   //Reset the SpinBox
        {
            ui->dsb_Mass->blockSignals(true);
            ui->dsb_Mass->setValue(oldMass / 100);
            ui->dsb_Mass->blockSignals(false);
            return;
        }
    }

    for (int i = 0; i < c_cpy->ingredients.length(); ++i)
    {
    //Calculate propotion on whole coctail
        double prop = (100/oldMass)  * c_cpy->ingredients.value(i).Mass;
        double propPart = deltaMass * (prop / 100);
        //c_cpy->ingredients.
        c_cpy->ingredients[i].Mass += propPart;
    }

    for(MixingDialogWidget *w : ingredientPanels)
        w->updateIngredientMass();


}

void MixingDialog::on_pb_reset_clicked()
{
    delete(c_cpy);
    c_cpy = new Cocktail(*c);
    loadCocktail();
}
