

#ifndef FB_COCKTAIL_H
#define FB_COCKTAIL_H

#include "fbc_db.h"

#include <QListWidget>
#include <QMainWindow>

#include <components/category.h>
#include <components/unit.h>
#include <components/decoration.h>
#include <components/ingredient.h>
#include <components/cocktail.h>

#include "homeInvetory.h"

namespace Ui {
class FB_Cocktail;
}
enum selectables {Cocktails, Ingredients, Decorations, Units, FilterResult};

class FB_Cocktail : public QMainWindow
{
    Q_OBJECT

public:
    explicit FB_Cocktail(QWidget *parent = 0);

    ~FB_Cocktail();

private slots:
    void on_btn_tb_Cocktails_clicked();
    void on_btn_tb_Ingredients_clicked();
    void on_btn_tb_possibleRecepies_clicked();
    void on_btn_tb_possibleOnAuto_clicked();
    void on_btn_tb_homebar_clicked();
    void on_btn_tb_filter_clicked();
    void on_btn_tb_slots_clicked();
    void on_btn_tb_settings_clicked();
    void on_pbMixing_clicked();


    void on_act_ShoppingList_triggered();
    void on_act_db_Cocktails_triggered();
    void on_act_db_Ingredients_triggered();
    void on_act_db_Categories_triggered();
    void on_act_db_units_triggered();
    void on_act_db_decoration_triggered();

    void on_lw_mainList_currentItemChanged(QListWidgetItem *current,QListWidgetItem *previous);

    void on_leSearch_textChanged(const QString);

private:
    Ui::FB_Cocktail *ui;
    fbc_DB *db;
    selectables selectedList;
    QList<Cocktail *> tmpList;
    //Lists

    void reloadList(selectables item);
};

#endif // FB_COCKTAIL_H
