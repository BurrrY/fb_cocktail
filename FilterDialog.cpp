/**
* $Id: FilterDialog.cpp 101 2014-07-15 17:32:45Z fbury $
* \file
* \brief This Class Handels the Filter-Form
* $Author: fbury $
* $Revision: 101 $
*/

#include "FilterDialog.h"
#include "fb_cocktail.h"

FilterDialog::FilterDialog(fbc_DB *pDB, QList<Cocktail*>  *pResult, QWidget * parent) : QDialog(parent) {
    setupUi(this);

    connect(this, SIGNAL(accepted()), this, SLOT(myAccept()));

    connect(cbA1, SIGNAL(toggled(bool)), this, SLOT(cb1Toggled(bool)));
    connect(cbA2, SIGNAL(toggled(bool)), this, SLOT(cb2Toggled(bool)));
    connect(cbA3, SIGNAL(toggled(bool)), this, SLOT(cb3Toggled(bool)));
    connect(cbA4, SIGNAL(toggled(bool)), this, SLOT(cb4Toggled(bool)));

    type.push_back(cbM1);
    type.push_back(cbM2);
    type.push_back(cbM3);
    type.push_back(cbM4);
    ingredients.push_back(cbI1);
    ingredients.push_back(cbI2);
    ingredients.push_back(cbI3);
    ingredients.push_back(cbI4);
    checker.push_back(cbA1);
    checker.push_back(cbA2);
    checker.push_back(cbA3);
    checker.push_back(cbA4);


    db = pDB;
    result = pResult;
    QList<Ingredient *> theList = Ingredient::getListName(db)->values();


    for(Ingredient *kv : theList) {
        QString item = kv->Name;
		cbI1->addItem(item);
		cbI2->addItem(item);
		cbI3->addItem(item);
		cbI4->addItem(item);
	}


	cbM1->addItem("mit");
	cbM1->addItem("ohne");

	cbM2->addItem("mit");
	cbM2->addItem("ohne");

	cbM3->addItem("mit");
	cbM3->addItem("ohne");

	cbM4->addItem("mit");
	cbM4->addItem("ohne");


	cbM1->setEnabled(false);
	cbM2->setEnabled(false);
	cbM3->setEnabled(false);
	cbM4->setEnabled(false);

	cbI1->setEnabled(false);
	cbI2->setEnabled(false);
	cbI3->setEnabled(false);
	cbI4->setEnabled(false);

}


void FilterDialog::myAccept()
{
    QList<Cocktail *> theCocList = Cocktail::getListID(db)->values();

	//create ResultList
    QList<Cocktail*> resultList;
    for(Cocktail *i : theCocList) {
        resultList.append(i);
	}

	//Filter by ingredients
	for(int i = 0;i<4;++i) {
		if(checker[i]->checkState() == Qt::Checked) {
			bool desired = (type[i]->currentText() == "mit");
            QString ingName = ingredients[i]->currentText();

            for(Cocktail * i : resultList) {
                if(i->hasIngredient(ingName) != desired) {
                    resultList.removeAt(resultList.indexOf(i));
				}
			}
		}
	}

	//Filter by Alk-Free





    result->clear();

    for(auto i : resultList)
        result->append(i);


    qDebug() << "Filter-Result: " << result->size();
}


void FilterDialog::cb1Toggled(bool state) {
	cbM1->setEnabled(state);
	cbI1->setEnabled(state);
}


void FilterDialog::cb2Toggled(bool state) {
	cbM2->setEnabled(state);
	cbI2->setEnabled(state);
}


void FilterDialog::cb3Toggled(bool state) {
	cbM3->setEnabled(state);
	cbI3->setEnabled(state);
}


void FilterDialog::cb4Toggled(bool state) {
	cbM4->setEnabled(state);
	cbI4->setEnabled(state);
}
