#ifndef FBC_DB_H
#define FBC_DB_H

#include <QFile>
#include <QFileInfo>
#include <QSqlDatabase>


typedef QMap<QString, QVariant> FBC_DataRow;
typedef QList<FBC_DataRow> FBC_DataSet;

class fbc_DB
{
public:
    fbc_DB();
    bool connnect();

    FBC_DataSet getData(QString query);
    bool exec(QString query);
};


#endif // FBC_DB_H
