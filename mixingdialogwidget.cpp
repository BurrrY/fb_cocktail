#include "mixingdialogwidget.h"
#include "ui_mixingdialogwidget.h"

MixingDialogWidget::MixingDialogWidget(CocktailIngredient *i, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MixingDialogWidget)
{

    ui->setupUi(this);
    this->i = i;

    ui->le_Title->setText(i->ingredient->Name);

    ui->dsb_MassCL->blockSignals(true);
    ui->dsb_MassCL->setValue(i->Mass);
    ui->dsb_MassCL->blockSignals(false);
}

MixingDialogWidget::~MixingDialogWidget()
{
    delete ui;
}

void MixingDialogWidget::setNr(int nr)
{
    ui->la_No->setText(QString("%1").arg(nr, 2, 10, QChar('0')));
}

void MixingDialogWidget::on_dsb_MassCL_valueChanged(double val)
{
    i->Mass = val;
    emit massValueChanged(val, i);
}

void MixingDialogWidget::updateIngredientMass() {
    ui->dsb_MassCL->blockSignals(true);
    ui->dsb_MassCL->setValue(i->Mass);
    ui->dsb_MassCL->blockSignals(false);

}
