#ifndef SLOTDIALOG_H
#define SLOTDIALOG_H

#include <QDialog>
#include <components/slot.h>
#include "fbc_db.h"
#include "fbc_dbg.h"

namespace Ui {
class SlotDialog;
}

class SlotDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SlotDialog(fbc_DB *db, QWidget *parent = 0);
    ~SlotDialog();

private:
    Ui::SlotDialog *ui;
    QList<Slot> Slots;
    int numSlots;
};

#endif // SLOTDIALOG_H
