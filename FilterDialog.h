#ifndef __FILTER_DIALOG__
#define __FILTER_DIALOG__

#include <QDialog>
#include <QMessageBox>
#include <QCheckBox>
#include <QComboBox>

#include <map>

#include "ui_Filter.h"

#include "components/ingredient.h"
#include "components/cocktail.h"

class FilterDialog : public QDialog, public Ui_FilterDialog {
    Q_OBJECT

	public:
        FilterDialog(fbc_DB *pDB, QList<Cocktail *> *pResult, QWidget * parent = 0);

    private slots:
		void cb1Toggled(bool);
		void cb2Toggled(bool);
		void cb3Toggled(bool);
		void cb4Toggled(bool);
		void myAccept();
    private:
        fbc_DB *db; /**< Database-Connection */
        QList<QComboBox*> type;
        QList<QComboBox*> ingredients;
        QList<QCheckBox*> checker;
        QList<Cocktail* > *result;
};

#endif
