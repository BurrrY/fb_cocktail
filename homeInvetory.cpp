#include "homeInvetory.h"

InventoryDialog::InventoryDialog(fbc_DB *pDB, QWidget * parent) : QDialog(parent) {

    setupUi(this);
    db = pDB;
    IngList = Ingredient::getListName(db);
    connect(listAll, SIGNAL(itemActivated( QListWidgetItem *)), this, SLOT(btnAddIngredient()));  
    connect(pbAdd, SIGNAL(clicked()), this, SLOT(btnAddIngredient()));  
    connect(pbDelete, SIGNAL(clicked()), this, SLOT(btnDelIngredient()));    
    connect(pbClear, SIGNAL(clicked()), this, SLOT(btnClearList()));    
		
	loadLists();
    // perform additional setup here ...
}


void InventoryDialog::loadLists() {
	int cntAll=0;
	int cntAdded=0;
	listAll->clear();
	listAdded->clear();
    QSet<int> added;
    QSet<int>::iterator ita;
	
    FBC_DataSet data = db->getData("SELECT zut_name, zut_id  FROM coc_zutaten, coc_flaschenmagazin WHERE coc_flaschenmagazin.fla_modul=1 AND coc_flaschenmagazin.fla_zutat=zut_id ORDER BY zut_name ASC");
     for(FBC_DataRow r: data) {
        listAdded->addItem(r["zut_name"].toString());
        added.insert(r["zut_id"].toInt());
		++cntAdded;		
	}	
	
    QList<Ingredient *> ingredients = IngList->values();
    for(Ingredient *i: ingredients) {

        if(added.contains(i->ID)) continue;

        listAll->addItem(i->Name);
		
		++cntAll;		
	} 
	
	labelCount->setText(QString::number(cntAll) + "/" + QString::number(cntAdded) );
}


void InventoryDialog::btnClearList() {
	db->exec("DELETE FROM coc_flaschenmagazin WHERE fla_modul = 1");
	loadLists();
}

	

void InventoryDialog::btnAddIngredient() {
	if(listAll->currentItem() == NULL) return;
	
    QString item = listAll->currentItem()->text();
	
    if(!IngList->contains(item)) {
        QMessageBox::critical(NULL, "Fehler!", ("SELECT zut_id FROM coc_zutaten WHERE zut_name = '" + 	item.toStdString() + "' ").c_str());
        return;
    }

    db->exec("INSERT INTO coc_flaschenmagazin (fla_level, fla_modul, fla_zutat) VALUES(1000, 1, " + QString::number(IngList->value(item)->ID) + ")");

	loadLists();
}



void InventoryDialog::btnDelIngredient() {
	if(listAdded->currentItem() == NULL) return;
	
	QString item = listAdded->currentItem()->text();
    FBC_DataSet res = db->getData("SELECT zut_id FROM coc_zutaten WHERE zut_name = '" + item + "' ");

    db->exec("DELETE FROM coc_flaschenmagazin WHERE fla_modul = 1 AND fla_zutat = " + res[0][0].toString() + "");
    labelCount->setText(item);
	loadLists();
}
